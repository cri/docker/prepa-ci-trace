#!/bin/sh

if [ -z "$OUTPUT_TRACE" ]; then
    echo "No ouput specified"
    exit 1
fi

# ensure the student can't read the test-suite
chmod go-rwx -R "test-suite"

# change owner of trace files
chown "runner:${RUNNER_GID}" -R "student"

student_folder=`pwd`/student

# execute trace
cd test-suite

exec env -i RUNNER_UID="${RUNNER_UID}" RUNNER_GID="${RUNNER_GID}" \
     OUTPUT_TRACE="${OUTPUT_TRACE}" PATH="$PATH" ./trace "$student_folder"
