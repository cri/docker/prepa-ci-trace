FROM python:rc-alpine

ARG RUNNER_UID=1000
ARG RUNNER_GID=1000

ENV RUNNER_UID=$RUNNER_UID
ENV RUNNER_GID=$RUNNER_GID

WORKDIR /prepa_ci/trace
COPY command.sh .


RUN addgroup runner -S -g $RUNNER_GID && adduser runner -S -u $RUNNER_UID -G runner

RUN pip3 install pipenv

COPY Pipfile .
COPY Pipfile.lock .
COPY install.sh .
RUN ["./install.sh"]

CMD ["/prepa_ci/trace/command.sh"]
