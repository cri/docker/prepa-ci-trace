# Prepa CI Trace

This docker image is used to generate traces by prepa_ci.

## Requirements

* The docker must be run with `--network none`.
* The testsuite execute must take as argument the folder of the student.
* The testsuite must have a `trace` executable at its root.
* The testsuite must output his results in the file `OUTPUT_TRACE` passed in the
  environement
* The testsuite must drop his privilege before executing any student code to
  `RUNNER_UID` and `RUNNER_GID` passed in the environement. If any file of the
  runner (like output file / configuration file) It needs to be open before the switch.

## Information

The runner must bind folder to `/prepa_ci/trace/student` and
`prepa_ci/trace/test-suite` that contains respectively the student code and
the testsuite.

The runner must not download any dependencies as the docker will not have access
to the network. If It needs any dependencies the docker image should have it
installed beforehand. Feel free to open an issue/MR to have things intalled.

## Environement variables

### Build

* `RUNNER_UID`: The UID of the user used by the runner, defaults to `1000`.
* `RUNNER_GID`: The GID used by the runner, defaults to `1000`.


### Required

* `OUTPUT_TRACE`: The path in which the output is located.
